﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MissileController : Projectile
{
    [Header("Misile Data")]
    [SerializeField] private Vector3 normalForce;
    [SerializeField] private Vector3 collidingForce;
    [SerializeField] private Vector3 wallForce;
    [NonSerialized] private RaycastHit hit;
    [NonSerialized] private bool wallColliding, floorColiding;
    [NonSerialized] private Quaternion initialRotation;

    [Header("Components")]
    [NonSerialized] private Rigidbody rb;


    private void Awake()
    {
        rb = GetComponent<Rigidbody>();
    }

    void Start()
    {
        wallColliding = false;
        floorColiding = false;
        initialRotation = transform.rotation;
        Physics.IgnoreLayerCollision(9, 9);
    }

    // Handles wall detection and rotation adjustment
    public override void Update()
    {
        base.Update();
        if (Physics.Raycast(transform.position,Vector3.right, out hit, 3))
        {
            transform.localRotation = new Quaternion(transform.localRotation.x,transform.localRotation.y,hit.collider.GetComponent<Transform>().localRotation.z-initialRotation.z,transform.localRotation.w);
            wallForce = new Vector3(wallForce.x, hit.collider.GetComponent<Transform>().rotation.z*30,wallForce.z );
            wallColliding = true;
        }
        else
        {
            wallColliding = false;
        }
    }

    // Applies forces based on collision state
    void FixedUpdate()
    {
        if (wallColliding)
        {
            rb.AddForce(wallForce);
        }
        else
        {
            transform.rotation = initialRotation;
            if (floorColiding)
            {
                rb.AddForce(collidingForce);
            }
            else
            {
                rb.AddForce(normalForce);
            }
        }
    }

    internal override void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Floor"))
        {
            floorColiding = true;
            normalForce = new Vector3(normalForce.x, -10, normalForce.z);
        }
    }

    private void OnCollisionExit(Collision collision)
    {
        if (collision.gameObject.CompareTag("Floor"))
        {
            floorColiding = false;
            normalForce = new Vector3(normalForce.x, -30, normalForce.z);
        }
    }

}
