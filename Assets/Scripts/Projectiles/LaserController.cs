﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using UnityEngine.UIElements;

public class LaserController : Projectile
{
    [Header("Laser Data")]
    [SerializeField]  public float maxGrowth;
    [SerializeField] public float laserSpeed;
    [NonSerialized] public bool relase = false;
    [NonSerialized] private Vector3 maxGrowthVector;
    [NonSerialized] private float relaseCounter = 0.2f;

    [Header("References")]
    [NonSerialized] public GameObject parent;

    [Header("Components")]
    [NonSerialized] private Rigidbody rb;
    [NonSerialized] private SphereCollider sphereCollider;

    private void Awake()
    {
        rb = GetComponent<Rigidbody>();
        sphereCollider = GetComponent<SphereCollider>();
    }

    void Start()
    {
        maxGrowthVector = new Vector3(transform.localScale.x + maxGrowth, transform.localScale.y + maxGrowth, transform.localScale.z + maxGrowth);
    }

    // Handles growth and collider activation
    public override void Update()
    {
        base.Update();
        relaseCounter -= Time.deltaTime;
        if (relaseCounter <= 0)
        {
            relase = true;
            relaseCounter = 0.2f;
        }
        if (!relase)
        {
            sphereCollider.enabled = false;
        }
        else
        {
            sphereCollider.enabled = true;
        }
    }

    // Handles growth and movement
    void FixedUpdate()
    {
        if (!relase)
        {
            if (transform.localScale.magnitude < maxGrowthVector.magnitude)
            {
                transform.localScale = new Vector3(transform.localScale.x+0.1f, transform.localScale.y + 0.1f, transform.localScale.z + 0.1f);
            }
            transform.position = parent.transform.position;
        }
        else
        {
            rb.velocity = new Vector3(laserSpeed, 0, 0);
        }
    }

}
