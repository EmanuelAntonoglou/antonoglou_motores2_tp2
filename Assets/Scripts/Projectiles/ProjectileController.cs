﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProjectileController : Projectile
{
    [Header("Projectile Data")]
    [SerializeField] public float projectileSpeed;

    [Header("Components")]
    [NonSerialized] public Rigidbody rb;

    private void Awake()
    {
        rb = GetComponent<Rigidbody>();
    }

    public override void Update()
    {
        base.Update();
        ProjectileDirection();
    }

    /// <summary>
    /// Move the projectile forward within a speed
    /// </summary>
    public virtual void ProjectileDirection()
    {
        rb.velocity = new Vector3(1 * projectileSpeed, rb.velocity.y, 0);
    }
}
