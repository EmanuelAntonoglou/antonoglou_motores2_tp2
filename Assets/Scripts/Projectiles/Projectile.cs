﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Projectile : MonoBehaviour
{
    [Header("Destroy Data")]
    [NonSerialized] private float xLimit = 29;
    [NonSerialized] private float yLimit = 50;
    
    virtual public void Update()
    {
        CheckLimits();
    }

    //Destroy if wall or floor collision
    internal virtual void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Wall") || collision.gameObject.CompareTag("Floor"))
        {
            Destroy(gameObject);
        }
    }

    /// <summary>
    /// Destroy if out of limits
    /// </summary>
    internal virtual void CheckLimits()
    {
        if (transform.position.x > xLimit)
        {
            Destroy(gameObject);
        }
        else if (transform.position.x < -xLimit)
        {
            Destroy(gameObject);
        }
        else if (transform.position.y > yLimit)
        {
            Destroy(gameObject);
        }
        else if (transform.position.y < -yLimit)
        {
            Destroy(gameObject);
        }
    }

}
