﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoubleProjectileController : ProjectileController
{
    [Header("Double Projectile Data")]
    [NonSerialized] public bool directionUp;

    public override void Update()
    {
        base.Update();
    }

    // Sets initial launch direction and rotation
    public override void ProjectileDirection()
    {
        if (directionUp)
        {
            rb.velocity = new Vector3(1 * projectileSpeed, 1 * projectileSpeed, 0);
            transform.rotation = Quaternion.Euler(0, 0, 50);
        }
        else
        {
            rb.velocity = new Vector3(1 * projectileSpeed, 1 * -projectileSpeed, 0);
            transform.rotation = Quaternion.Euler(0, 0, -50);
        }
    }

}
