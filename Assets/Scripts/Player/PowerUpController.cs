﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PowerUpController : Projectile
{
    [Header("PowerUp Data")]
    [SerializeField] private float vel = 1.5f;

    [Header("Components")]
    [NonSerialized] private Rigidbody rb;

    private void Awake()
    {
        rb = GetComponent<Rigidbody>();
    }

    private void FixedUpdate()
    {
        rb.velocity = new Vector3(-vel, 0,0);
    }
}
