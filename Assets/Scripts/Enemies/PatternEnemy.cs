﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PatternEnemy : EnemyController
{
    [Header("Enemy Data")]
    [NonSerialized] private bool goingUp;
    [NonSerialized] private bool forward;
    [NonSerialized] private float timer = 1f;

    [Header("Components")]
    [NonSerialized] private Rigidbody rb;

    override public void Start()
    {
        rb = GetComponent<Rigidbody>();
    }

    // Handles movement logic and timer for direction changes
    override public void Update()
    {
        base.Update();
        timer -= Time.deltaTime;
        if (timer < 0)
        {
            rb.velocity = Vector3.zero;
            if (forward)
            {
                forward = false;
            }
            else
            {
                goingUp = !goingUp;
                forward = true;
            }
            timer = 1f;
        }
    }

    // Applies forces based on current direction
    void FixedUpdate()
    {
        if (forward)
        {
            rb.AddForce(new Vector3(-1, 0, 0) * enemySpeed,ForceMode.Impulse);
        }
        else
        {
            if (goingUp)
            {
                rb.AddForce(new Vector3(-1, -1, 0) * enemySpeed, ForceMode.Impulse);
            }
            else
            {
                rb.AddForce(new Vector3(-1, 1, 0) * enemySpeed, ForceMode.Impulse);
            }
        }
    }

}
