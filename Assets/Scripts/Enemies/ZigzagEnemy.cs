﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ZigzagEnemy : EnemyController
{
    [Header("Enemy Data")]
    [NonSerialized] private bool goingUp;

    [Header("Components")]
    [NonSerialized] private Rigidbody rb;

    private void Awake()
    {
        rb = GetComponent<Rigidbody>();
    }

    // Applies bouncing movement
    void FixedUpdate()
    {
        if (goingUp)
        {
            rb.AddForce(new Vector3(-1, 1, 0) * enemySpeed);
        }
        else
        {
            rb.AddForce(new Vector3(-1, -1, 0) * enemySpeed);
        }
    }

    // Handles bouncing behavior on floor and ceiling
    internal override void OnCollisionEnter(Collision collision)
    {
        base.OnCollisionEnter(collision);
        if (collision.gameObject.CompareTag("Floor"))
        {
            goingUp = true;
            rb.velocity = Vector3.zero;
        }
        if (collision.gameObject.CompareTag("Ceiling"))
        {
            goingUp = false;
            rb.velocity = Vector3.zero;
        }
    }
}
