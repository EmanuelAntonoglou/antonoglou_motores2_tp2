﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowingEnemy : EnemyController
{
    [Header("Enemy Data")]
    [NonSerialized] private Vector3 direction;

    [Header("References")]
    [NonSerialized] private GameObject player;

    [Header("Components")]
    [NonSerialized] private Rigidbody rb;


    void Start()
    {
        if (Controller_Player._Player != null)
        {
            player = Controller_Player._Player.gameObject;
        }
        else
        {
            player = GameObject.Find("Player");
        }
        rb = GetComponent<Rigidbody>();
    }

    // Calculates direction towards player
    public override void Update()
    {
        base.Update();
        if (player != null)
        {
            direction = -(transform.localPosition - player.transform.localPosition).normalized;
        }
    }

    // Applies force towards player
    void FixedUpdate()
    {
        if (player != null)
        {
            rb.AddForce(direction * enemySpeed);
        }
    }
}
