﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyController : MonoBehaviour
{
    [Header("Enemy Data")]
    [SerializeField] private float xLimit;
    [SerializeField] public float enemySpeed;
    [NonSerialized] private float shootingCooldown;

    [Header("Prefabs")]
    [SerializeField] private GameObject enemyProjectile;
    [SerializeField] private GameObject powerUp;

    public virtual void Start()
    {
        shootingCooldown = UnityEngine.Random.Range(1, 10);
    }

    public virtual void Update()
    {
        shootingCooldown -= Time.deltaTime;
        CheckLimits();
        ShootPlayer();
    }

    //Destroy itself on collision with Projectile
    internal virtual void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Projectile"))
        {
            GeneratePowerUp();
            Controller_Hud.points++;
            Destroy(collision.gameObject);
            Destroy(gameObject);
        }
        if (collision.gameObject.CompareTag("Laser"))
        {
            GeneratePowerUp();
            Controller_Hud.points++;
            Destroy(gameObject);
        }
    }

    /// <summary>
    /// Shoot towards player
    /// </summary>
    void ShootPlayer()
    {
        if (Controller_Player._Player != null)
        {
            if (shootingCooldown <= 0)
            {
                Instantiate(enemyProjectile, transform.position, Quaternion.identity);
                shootingCooldown = UnityEngine.Random.Range(1, 10);
            }
        }
    }

    /// <summary>
    /// Destroy the enemy if off limits
    /// </summary>
    private void CheckLimits()
    {
        if (transform.position.x < xLimit)
        {
            Destroy(gameObject);
        }
    }

    /// <summary>
    /// Instantiate a PowerUp Random
    /// </summary>
    private void GeneratePowerUp()
    {
        int rnd = UnityEngine.Random.Range(0, 3);
        if (rnd == 2)
        {
            Instantiate(powerUp, transform.position, Quaternion.identity);
        }
    }
}
