﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyInstantiator : MonoBehaviour
{
    [Header("Instantiator Data")]
    [SerializeField] private float spawnTime = 7;
    [SerializeField] private List<GameObject> enemiesPrefabs;
    [NonSerialized] private Vector3 instantiatePos;
    [NonSerialized] private float time = 0;
    [NonSerialized] private int multiplier = 20;


    private void Start()
    {
        instantiatePos = transform.position;
    }

    void Update()
    {
        spawnTime -= Time.deltaTime;
        SpawnenemiesPrefabs();
        ChangeVelocity();
    }

    // Increase enemy speed over time
    private void ChangeVelocity()
    {
        time += Time.deltaTime;
        if (time > multiplier)
        {
            multiplier *= 2;
        }
    }

    // Spawns enemies periodically
    private void SpawnenemiesPrefabs()
    {
        if (spawnTime <= 0)
        {
            float offsetX = instantiatePos.x;
            int rnd = UnityEngine.Random.Range(0, enemiesPrefabs.Count);
            for (int i = 0; i < 5; i++)
            {
                offsetX = offsetX + 4;
                Vector3 pos = new Vector3(offsetX, instantiatePos.y, instantiatePos.z);
                Instantiate(enemiesPrefabs[rnd], pos, Quaternion.identity, gameObject.transform);
            }
            spawnTime = 7;
        }
    }

}
