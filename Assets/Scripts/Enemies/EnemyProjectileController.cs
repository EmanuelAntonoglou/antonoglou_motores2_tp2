﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyProjectileController : Projectile
{
    [Header("Enemy Projectile Data")]
    [SerializeField] public float enemyProjectileSpeed;
    [NonSerialized] private Vector3 direction;

    [Header("References")]
    [NonSerialized] private GameObject player;

    [Header("Components")]
    [NonSerialized] private Rigidbody rb;

    private void Awake()
    {
        rb = GetComponent<Rigidbody>();
    }

    void Start()
    {
        if (Controller_Player._Player != null)
        {
            player = Controller_Player._Player.gameObject;
            direction = -(transform.localPosition - player.transform.localPosition).normalized;
        }
    }

    // Moves Projectile towards Player 
    public override void Update()
    {
        base.Update();
        rb.AddForce(direction * enemyProjectileSpeed);
    }

}
