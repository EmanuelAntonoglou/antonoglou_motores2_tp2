using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class JumperEnemy : EnemyController
{
    [Header("Jumper Enemy Data")]
    [SerializeField] private float jumpForce;
    [SerializeField] private float jumpInterval;
    [NonSerialized] private float nextJumpTime = 0f;

    [Header("Growth Data")]
    [SerializeField] private float minScale = 1f;
    [SerializeField] private float maxScale = 1.5f;
    [SerializeField] private float growShrinkSpeed = 0.1f;
    [NonSerialized] private float currentScale = 1f;
    [NonSerialized] private bool isGrowing = true;

    [Header("Components")]
    [NonSerialized] private Rigidbody rb;

    private void Awake()
    {
        rb = GetComponent<Rigidbody>();
    }

    override public void Start()
    {
        base.Start();
        nextJumpTime = Time.time + jumpInterval;
    }

    override public void Update()
    {
        base.Update();

        if (Time.time >= nextJumpTime)
        {
            bool jumpUp = UnityEngine.Random.Range(0, 2) == 1;
            Vector3 jumpDirection = jumpUp ? Vector3.up : Vector3.down;
            rb.AddForce(jumpDirection * jumpForce, ForceMode.Impulse);
            nextJumpTime = Time.time + jumpInterval + UnityEngine.Random.Range(0f, jumpInterval * 0.5f);
        }
    }

    void FixedUpdate()
    {
        rb.AddForce(new Vector3(-1, 0, 0) * enemySpeed, ForceMode.Impulse);
        currentScale += (isGrowing ? growShrinkSpeed : -growShrinkSpeed) * Time.deltaTime;
        currentScale = Mathf.Clamp(currentScale, minScale, maxScale);
        transform.localScale = new Vector3(currentScale, currentScale, currentScale);
        if (currentScale == minScale || currentScale == maxScale)
        {
            isGrowing = !isGrowing;
        }
    }
}